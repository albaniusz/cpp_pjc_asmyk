/*
 * Napisz program, który wyświetli ciąg znaków "Hello world"
 *
 * Version: $Id: main.cpp 199 2012-03-26 18:13:58Z s8376@pjwstk.edu.pl $
 */
#include <iostream>
using namespace std;

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}

