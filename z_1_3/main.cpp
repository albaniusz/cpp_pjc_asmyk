/*
 * Co będzie wynikiem działania poniższego programu - operacje poprzednikowe i następnikowe.
 *
 * Version: $Id: main.cpp 199 2012-03-26 18:13:58Z s8376@pjwstk.edu.pl $
 */
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
    int f1 = 10, f2 = 20;

    cout << "Linia 1  " << f1++ + f2++ << endl;
    cout << "Linia 2  " << f1 << "," << f2++ << endl;
    f1 = 10;
    f2 = 20;
    cout << "Linia 3  " << ++f1 + ++f2 << endl;
    cout << "Linia 4  " << f1 << "," << f2++ << endl;

    return 0;
}

