/*
 * deklaracje.h
 *
 * Version: $Id: deklaracje.h 203 2012-03-27 19:20:20Z s8376@pjwstk.edu.pl $
 */
#ifndef DEKLARACJE_H_
#define DEKLARACJE_H_

#include <iostream>
#include <math.h>

#include "definicje.cpp"

bool isPrimeNumber(int);
long sumaLiczbPierwszych(int);

#endif /* DEKLARACJE_H_ */

