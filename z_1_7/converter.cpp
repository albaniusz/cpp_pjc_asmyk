/*
 * converter.cpp
 *
 * @version $Id: converter.cpp 209 2012-04-01 19:43:29Z s8376@pjwstk.edu.pl $
 */
#include <iostream>
#include <cstdio>
using namespace std;
 
string convert(int number, int base)
{
    int mark;
    string result = "";
    string tmp;
                 
    while (number > 0) {
        mark = number % base;
        if (mark > 9) {
            result = char(97 - 10 + mark) + result;
        } else {
            sprintf((char*) tmp.c_str(), "%d", mark);
            result = tmp.c_str() + result;
        }
                                                                                                     
        number = number / base;
    }
    
    return result;
 }

