/*
 * converter.h
 *
 * @version $Id: converter.h 209 2012-04-01 19:43:29Z s8376@pjwstk.edu.pl $
 */
#ifndef CONVERTER_H_
#define CONVERTER_H_

#include <iostream>
std::string convert(int, int);

#endif /* CONVERTER_H_ */

